# st

my build of st (suckless simple terminal), with scrolling, transparency, and copy/paste functionality.

# Installation

**install the needed xlib header files**

`sudo apt install libx11-dev`

**clone the directory**

`git clone https://gitlab.com/scary90/st.git`

**install**

`sudo make clean install`
